package com.dedi.qbitapps.activities;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dedi.qbitapps.R;

import java.lang.reflect.Field;

public class MainActivity extends AppCompatActivity {

    private Fragment selectedFragment = HomeFragment.newInstance();
    private int selectMenu = 0;

    private Menu bottomMenu;
    private BottomNavigationView bottomNavigationView;

    public static TextView textCartItemCount;
    public static int mCartItemCount = 0 ;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.action_cart);

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        mCartItemCount = 0;

//        actionView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onOptionsItemSelected(menuItem);
//            }
//        });

        return true;
    }

    public static void setupBadge() {

        Log.e("count",""+mCartItemCount);

        if (textCartItemCount != null) {
            textCartItemCount.setText(String.valueOf(mCartItemCount));
            if (textCartItemCount.getVisibility() != View.VISIBLE) {
                textCartItemCount.setVisibility(View.VISIBLE);
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_cart: {
                // Do something
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setItemIconTintList(null);
        BottomNavigationHelper helper = new BottomNavigationHelper();
        helper.removeShiftItem(bottomNavigationView);
        bottomMenu = (Menu)bottomNavigationView.getMenu();
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                        bottomMenu.getItem(0).setIcon(R.drawable.ic_home_black);
                        bottomMenu.getItem(1).setIcon(R.drawable.ic_favorite_black);
                        bottomMenu.getItem(2).setIcon(R.drawable.ic_person_black);
                        switch (item.getItemId()) {
                            case R.id.id_tab_home:
                                item.setIcon(R.drawable.ic_home_red);
                                if (selectMenu!=item.getItemId()){
                                    selectedFragment = HomeFragment.newInstance();
                                }
                                break;
                            case R.id.id_tab_fav:
                                item.setIcon(R.drawable.ic_favorite_red);
                                if (selectMenu!=item.getItemId()){
                                    selectedFragment = FavouriteFragment.newInstance();
                                }
                                break;
                            case R.id.id_tab_about:
                                item.setIcon(R.drawable.ic_person_red);
                                if (selectMenu!=item.getItemId()){
                                    selectedFragment = AboutFragment.newInstance();
                                }
                                break;
                        }
                        selectMenu = item.getItemId();

                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        if (selectedFragment==null){
                            transaction.replace(R.id.viewpager, HomeFragment.newInstance());

                            item.setIcon(R.drawable.ic_home_red);
                            selectMenu = R.id.id_tab_home;
                        }else{
                            transaction.replace(R.id.viewpager, selectedFragment);
                        }
                        transaction.commit();
                        return true;
                    }
                });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (selectedFragment==null){
            transaction.replace(R.id.viewpager, HomeFragment.newInstance());
            selectMenu = R.id.id_tab_home;
            bottomMenu.getItem(0).setIcon(R.drawable.ic_home_red);
        }else{
            transaction.replace(R.id.viewpager, selectedFragment);
        }
        transaction.commit();

    }

    private class BottomNavigationHelper {

        @SuppressLint("RestrictedApi")
        private void removeShiftItem(BottomNavigationView view){
            BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
            try {
                Field shiftMode = menuView.getClass().getDeclaredField("mShiftingMode");
                shiftMode.setAccessible(true);
                shiftMode.setBoolean(menuView, false);
                shiftMode.setAccessible(false);

                for (int i=0; i<menuView.getChildCount(); i++){
                    BottomNavigationItemView itemView = (BottomNavigationItemView)menuView.getChildAt(i);
                    itemView.setShifting(true);
                    itemView.setChecked(itemView.getItemData().isChecked());


                    final View iconView = menuView.getChildAt(i).findViewById(android.support.design.R.id.icon);
                    final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
                    final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                    layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 18, displayMetrics);
                    layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 18, displayMetrics);
                    iconView.setLayoutParams(layoutParams);

                }
            }catch (NoSuchFieldException e){

            }catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
