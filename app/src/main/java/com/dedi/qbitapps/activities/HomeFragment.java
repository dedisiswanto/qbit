package com.dedi.qbitapps.activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.dedi.qbitapps.R;
import com.dedi.qbitapps.adapter.ProductAdapter;
import com.dedi.qbitapps.model.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    List<Product> productList;

    SliderLayout mDemoSlider;

    RecyclerView recyclerView;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        mDemoSlider = (SliderLayout) v.findViewById(R.id.slider_image);

        recyclerView = (RecyclerView) v.findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        productList = new ArrayList<>();

        productList.add(
                new Product(
                        1,
                        "Nike Classic Cortez",
                        R.drawable.nike1));

        productList.add(
                new Product(
                        2,
                        "Nike Air Max 270",
                        R.drawable.nike2));

        productList.add(
                new Product(
                        3,
                        "Nike Air Force 1 Shadow",
                        R.drawable.nike3));

        ProductAdapter adapter = new ProductAdapter(getActivity(), productList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        HashMap<String,String> file_maps = new HashMap<String, String>();
        file_maps.put("Promo 1", "https://kartukredit.bri.co.id/public/uploads/promotion/nike-factory-store-1530782725.jpg");
        file_maps.put("Promo 2", "https://www.static-src.com/siva/asset//01_2019/Nike-Clearance-microsite.jpg");
        file_maps.put("Promo 3", "https://cdn.dribbble.com/users/1177219/screenshots/7075922/final.png");

        for(String name : file_maps.keySet()){
            DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit);
//                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("link",file_maps.get(name));

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(5000);

        return v;
    }

}
