package com.dedi.qbitapps.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.dedi.qbitapps.R;
import com.dedi.qbitapps.activities.MainActivity;
import com.dedi.qbitapps.model.Product;

import java.util.List;

import static com.dedi.qbitapps.activities.MainActivity.mCartItemCount;
import static com.dedi.qbitapps.activities.MainActivity.setupBadge;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private Context context;
    private List<Product> productList;

    public ProductAdapter(Context context, List<Product> productList) {
        this.context = context;
        this.productList = productList;
    }


    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_product, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, int position) {
        final Product product = productList.get(position);

        holder.textViewTitle.setText(product.getTitle());

        holder.imageView.setImageDrawable(context.getResources().getDrawable(product.getImage()));

        holder.btn_add.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_add));
        holder.btn_add.setChecked(true);
        holder.btn_add.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if (isChecked){
                    holder.btn_add.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_add));
                    Toast.makeText(context, "Deleted Item : "+product.getTitle(), Toast.LENGTH_SHORT).show();
                    mCartItemCount = mCartItemCount -1;
                    setupBadge();
                } else{
                    holder.btn_add.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_clear));
                    Toast.makeText(context, "Added Item : "+product.getTitle(), Toast.LENGTH_SHORT).show();
                    mCartItemCount = mCartItemCount +1;
                    setupBadge();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle;
        ImageView imageView;
        ToggleButton btn_add;

        public ProductViewHolder(View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.tv_title);
            imageView = itemView.findViewById(R.id.iv_product);
            btn_add = itemView.findViewById(R.id.btn_add);
        }
    }
}
